def format_list(records):
    response = ''
    for entry in records:
        link = f"<a href='/students/update/{entry.id}'>UPDATE</a>"
        response += '<br>' + link + ' ' + str(entry)
    return response if response else 'EMPTY RESULT'
