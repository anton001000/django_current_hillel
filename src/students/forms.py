from django.core.exceptions import ValidationError
from django.forms import ModelForm

from students.models import Student


SHORT_LENGTH = 13


class StudentBaseForm(ModelForm):

    class Meta():
        model = Student
        fields = '__all__'

    def clean_phone_number(self):
        return self.cleaned_data.get("phone_number")

    def clean_email(self):
        email = self.cleaned_data.get("email")

        students = Student.objects.filter(email=email).exclude(id=self.instance.id)

        if students:
            raise ValidationError("Student has already exist with this email")

        return email

    def clean(self):
        result = super().clean()

        enroll_date = self.cleaned_data['enroll_date']
        graduate_date = self.cleaned_data['graduate_date']

        if enroll_date > graduate_date:
            raise ValidationError("Enroll date couldn't be greater graduate date")

        return result


class StudentCreateForm(StudentBaseForm):
    pass


class StudentUpdateForm(StudentBaseForm):
    class Meta(StudentBaseForm.Meta):
        exclude = ['age']
