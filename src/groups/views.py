from django.db.models import Q
from django.http import HttpResponseRedirect
from django.shortcuts import render # noqa
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt

from groups.forms import GroupCreateForm, GroupUpdateForm
from groups.models import Groups


def get_groups(request):
    groups = Groups.objects.all()

    params = [
        'name',
        'department',
    ]
    for param_name in params:
        param_value = request.GET.get(param_name)
        if param_value:
            param_elems = param_value.split(',')
            if param_elems:
                or_filter = Q()
                for param_elem in param_elems:
                    or_filter |= Q(**{param_name: param_elem})
                groups = groups.filter(or_filter)
            else:
                groups = groups.filter(**{param_name: param_value})

    return render(
        request=request,
        template_name='groups-list.html',
        context={'groups': groups}
    )


@csrf_exempt
def create_groups(request):

    if request.method == 'POST':
        form = GroupCreateForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('list'))

    else:

        form = GroupCreateForm()

    return render(
        request=request,
        template_name='groups-create.html',
        context={'form': form}
    )


@csrf_exempt
def update_groups(request, id): # noqa

    group = Groups.objects.get(id=id) # noqa

    if request.method == 'POST':
        form = GroupUpdateForm(
            data=request.POST,
            instance=group
        )
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('list'))
    else:
        form = GroupUpdateForm(instance=group)

    return render(
        request=request,
        template_name='groups-update.html',
        context={'form': form}
    )
