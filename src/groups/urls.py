from django.urls import path

from groups.views import create_groups, get_groups, update_groups

urlpatterns = [
    path('', get_groups, name='list'),
    path('new', create_groups, name='create'),
    path('edit/<int:id>', update_groups, name='update'),
]
