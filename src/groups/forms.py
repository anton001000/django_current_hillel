from django.forms import ModelForm

from groups.models import Groups


class GroupBaseForm(ModelForm):

    class Meta():
        model = Groups
        fields = ['name', 'department']


class GroupCreateForm(GroupBaseForm):
    pass


class GroupUpdateForm(GroupBaseForm):
    class Meta(GroupBaseForm.Meta):
        exclude = ['name']
