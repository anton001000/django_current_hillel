from django.db import models


class Groups(models.Model):
    name = models.CharField(max_length=60, null=False)
    department = models.CharField(max_length=40, null=False)

    def __str__(self):
        return f'{self.name}, ' \
               f'{self.department}'
