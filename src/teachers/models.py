import random

from django.db import models

from faker import Faker

from core.validators import validate_email_for_prohibited_domain, validate_phone_number


class Teacher(models.Model):
    first_name = models.CharField(max_length=64, null=False)
    last_name = models.CharField(max_length=84, null=False)
    teaching_department = models.CharField(max_length=100, null=False)

    scholastic_degree = models.CharField(max_length=20)
    email = models.EmailField(max_length=64, validators=[
        validate_email_for_prohibited_domain
    ])

    birth_date = models.IntegerField(null=True)
    phone_number = models.CharField(null=False, max_length=20, validators=[
        validate_phone_number
    ])

    def __str__(self):
        return f'{self.first_name}, ' \
               f'{self.last_name}, ' \
               f'{self.teaching_department}, ' \
               f'{self.phone_number}, ' \
               f'{self.scholastic_degree}, ' \
               f'{self.email}, '

    @classmethod
    def generate_teachers(cls, count):
        faker = Faker()
        departments = [
            "THE DEPARTMENT OF ARTS & HUMANITIES",
            "THE DEPARTMENT OF COUNSELING & CLINICAL PSYCHOLOGY",
            "THE DEPARTMENT OF EDUCATION POLICY & SOCIAL ANALYSIS",
            "THE DEPARTMENT OF HUMAN DEVELOPMENT",
            "THE DEPARTMENT OF OTHER AREAS OF INTEREST",
            "THE DEPARTMENT OF MATHEMATICS, SCIENCE & TECHNOLOGY",
            "THE DEPARTMENT OF BIOBEHAVIORAL SCIENCES",
            "THE DEPARTMENT OF CURRICULUM & TEACHING",
            "THE DEPARTMENT OF HEALTH & BEHAVIOR STUDIES",
            "THE DEPARTMENT OF INTERNATIONAL & TRANSCULTURAL STUDIES",
            "THE DEPARTMENT OF ORGANIZATION & LEADERSHIP"
        ]

        degrees = [
            "bachelor's degrees",
            "first professional degrees",
            "master's degrees",
            "research doctorates"
        ]
        #  1980.01.01
        start_date = 315532800
        #  1990.01.01
        end_date = 631152000

        for _ in range(count):
            obj = cls(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                teaching_department=random.choice(departments),
                scholastic_degree=random.choice(degrees),
                email=faker.email(),
                birth_date=random.randint(start_date, end_date),
                phone_number=faker.phone_number()
            )
            obj.save()
