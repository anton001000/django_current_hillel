from django.db.models import Q
from django.http import HttpResponseRedirect
from django.shortcuts import render # noqa
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt

from teachers.forms import TeacherCreateForm, TeacherUpdateForm
from teachers.models import Teacher


def get_teachers(request):
    teachers = Teacher.objects.all()

    params = [
        'first_name',
        'first_name__startswith',
        'first_name__endswith',
        'last_name',
        'teaching_department',
        'scholastic_degree',
        'email',
        'birth_date',
        'phone_number'
    ]

    for param_name in params:
        param_value = request.GET.get(param_name)
        if param_value:
            param_elems = param_value.split(',')
            if param_elems:
                or_filter = Q()
                for param_elem in param_elems:
                    or_filter |= Q(**{param_name: param_elem})
                teachers = teachers.filter(or_filter)
            else:
                teachers = teachers.filter(**{param_name: param_value})

    return render(
        request=request,
        template_name='teachers-list.html',
        context={'teachers': teachers}
    )


@csrf_exempt
def create_teachers(request):
    if request.method == 'POST':
        form = TeacherCreateForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/teachers/')

    else:
        form = TeacherCreateForm()
    return render(
        request=request,
        template_name='teachers-create.html',
        context={'form': form}
    )


@csrf_exempt
def update_teachers(request, id): # noqa

    teacher = Teacher.objects.get(id=id) # noqa

    if request.method == 'POST':
        form = TeacherUpdateForm(
            data=request.POST,
            instance=teacher
        )
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('list'))
    else:
        form = TeacherUpdateForm(instance=teacher)

    return render(
        request=request,
        template_name='teachers-updates.html',
        context={'form': form}
    )
