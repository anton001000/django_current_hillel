from django.core.management.base import BaseCommand

from teachers.models import Teacher


class Command(BaseCommand):
    def handle(self, *args):
        Teacher.generate_teachers(int(args))
