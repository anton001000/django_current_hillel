def format_list(records):
    response = ''
    for entry in records:
        response += '<br>' + \
                    str(entry.first_name) + " " + \
                    str(entry.last_name) + " " + \
                    str(entry.teaching_department) + " " + \
                    str(entry.scholastic_degree)

    return response if response else 'EMPTY RESULT'
