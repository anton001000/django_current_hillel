from django.core.exceptions import ValidationError
from django.forms import ModelForm

from teachers.models import Teacher


class TeacherBaseForm(ModelForm):

    class Meta():
        model = Teacher
        fields = "__all__"

    def clean_phone_number(self):
        return self.cleaned_data.get("phone_number")

    def clean_email(self):
        email = self.cleaned_data.get("email")

        teacher = Teacher.objects.filter(email=email).exclude(id=self.instance.id)
        if teacher:
            raise ValidationError("Teacher has already exist with this email")

        return email


class TeacherCreateForm(TeacherBaseForm):
    pass


class TeacherUpdateForm(TeacherBaseForm):
    class Meta(TeacherBaseForm.Meta):
        exclude = ['age']
