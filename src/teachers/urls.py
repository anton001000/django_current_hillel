from django.urls import path

from teachers.views import create_teachers, get_teachers, update_teachers

urlpatterns = [
    path('', get_teachers, name='list'),
    path('new/', create_teachers, name='new'),
    path('edit/<int:id>', update_teachers, name='update'),
]
