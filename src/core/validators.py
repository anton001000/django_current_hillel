import re

from django.core.exceptions import ValidationError


def validate_email_for_prohibited_domain(value):
    white_list = [
        ".com",
        ".net",
        ".biz",
        ".info",
        ".org",
        ".tel",
        ".travel",
        ".aero",
        ".pro",
        ".name",
        ".mobi"
    ]
    valid = None
    _, _, email_domain = value.partition("@")
    for domain in white_list:
        if domain in email_domain:
            valid = True
            break

    if not valid:
        raise ValidationError("Domain of email is not correct")


def validate_phone_number(phone_number):
    pattern = r'\+\d{2}\(\d{3}\)\d{3}\-\d{4}'

    if not re.match(pattern, phone_number):
        raise ValidationError("Phone number is not correct. "
                              "Phone number has to be like that -> EXAMPLE: '+38(099)888-7766'")
